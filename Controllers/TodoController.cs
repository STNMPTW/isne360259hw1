using System.Collections.Generic;
using isne360259hw1.Migrations;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Models;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    public class TodoController : Controller
    {
      DB n = new DB();
      
      public TodoController(ITodoRepository todoItems)
      {
        TodoItems = todoItems;
      }
      public ITodoRepository TodoItems { get; set; }

      [HttpGet]
      public IEnumerable<TodoItem> GetAll()
      {
        return TodoItems.GetAll();
      }

      [HttpGet("{id}", Name = "GetTodo")]
      public IActionResult GetById(string id)
      {
        var item = TodoItems.Find(id);
        if (item == null)
        {
          return NotFound();
        }
        return new ObjectResult(item);
      }
      [HttpDelete("{id}",Name = "DeleteTodo")]
      public IActionResult RemoveById(string id)
      {
        var item = TodoItems.Remove(id);
        if(item == null)
        {
          return NotFound();
        }
        return new ObjectResult(item);
      }
      public IActionResult InsertByName(TodoItem name)
      {
        TodoItems.Add(name);
        return Ok();
      }
      //[HttpGet]
      // public IActionResult generate()
      // {
        
      //   TodoRepository l = new TodoRepository();
      //   foreach(TodoItem m in l.GetAll())
      //   {
      //     n.Add(m);
      //   }
      //   n.SaveChanges();
      //   return RedirectToAction("GetAll");
      // }

  }
}
