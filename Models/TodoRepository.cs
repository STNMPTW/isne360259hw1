using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace TodoApi.Models
{
    public class TodoRepository : ITodoRepository
    {
        DB database = new DB();
        private static ConcurrentDictionary<string, TodoItem> _todos =
              new ConcurrentDictionary<string, TodoItem>();
        public void FectData()
        {
            foreach(TodoItem a in database.todoes)
            {
                _todos[a.Key] = a;
            }
        }
        public TodoRepository()
        {
            //Add(new TodoItem { Name = "Item eiei" });
            this.FectData();
        }

        public IEnumerable<TodoItem> GetAll()
        {

            return _todos.Values;
        }

        public void Add(TodoItem item)
        {
            item.Key = Guid.NewGuid().ToString();
            database.Add(item);
            database.SaveChanges();
            FectData();
        }

        public TodoItem Find(string key)
        {
            TodoItem item;
            _todos.TryGetValue(key, out item);
            return item;
        }

        public TodoItem Remove(string key)
        {
            TodoItem item;
            _todos.TryRemove(key, out item);
            item.Key = key;
            database.todoes.Remove(item);
            database.SaveChanges();
            return item;
        }

        public void Update(TodoItem item)
        {
            _todos[item.Key] = item;
        }
    }
}
